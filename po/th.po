# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Theppitak Karoonboonyanan <theppitak@gmail.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: Thunar Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-02-15 00:30+0100\n"
"PO-Revision-Date: 2019-01-07 15:41+0000\n"
"Last-Translator: Theppitak Karoonboonyanan <theppitak@gmail.com>\n"
"Language-Team: Thai (http://www.transifex.com/xfce/thunar-plugins/language/th/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: th\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../thunar-plugin/tag-renamer.c:59 ../thunar-plugin/tag-renamer.c:659
msgid "Title"
msgstr "ชื่อเพลง"

#: ../thunar-plugin/tag-renamer.c:60
msgid "Artist - Title"
msgstr "ศิลปิน - ชื่อเพลง"

#: ../thunar-plugin/tag-renamer.c:61
msgid "Track - Title"
msgstr "ลำดับ - ชื่อเพลง"

#: ../thunar-plugin/tag-renamer.c:62
msgid "Track - Artist - Title"
msgstr "ลำดับ - ศิลปิน - ชื่อเพลง"

#: ../thunar-plugin/tag-renamer.c:63
msgid "Track. Title"
msgstr "ลำดับ. ชื่อเพลง"

#: ../thunar-plugin/tag-renamer.c:64
msgid "Track. Artist - Title"
msgstr "ลำดับ. ศิลปิน - ชื่อเพลง"

#: ../thunar-plugin/tag-renamer.c:65
msgid "Artist - Track - Title"
msgstr "ศิลปิน - ลำดับ - ชื่อเพลง"

#: ../thunar-plugin/tag-renamer.c:67
msgid "Custom"
msgstr "กำหนดเอง"

#. Custom format
#: ../thunar-plugin/tag-renamer.c:243
msgid "Cust_om format:"
msgstr "รูปแบบ_กำหนดเอง:"

#. Format label
#: ../thunar-plugin/tag-renamer.c:272
msgid "_Format:"
msgstr "รูปแ_บบ:"

#: ../thunar-plugin/tag-renamer.c:314
msgid "_Underscores"
msgstr "ขีด_ล่าง"

#: ../thunar-plugin/tag-renamer.c:316
msgid ""
"Activating this option will replace all spaces in the target filename with "
"underscores."
msgstr "ตัวเลือกนี้จะแทนที่อักขระเว้นวรรคทั้งหมดในชื่อปลายทางด้วยขีดล่าง"

#: ../thunar-plugin/tag-renamer.c:321
msgid "_Lowercase"
msgstr "ตัวพิมพ์เล็_ก"

#: ../thunar-plugin/tag-renamer.c:323
msgid ""
"If you activate this, the resulting filename will only contain lowercase "
"letters."
msgstr "ถ้าเลือกตัวเลือกนี้ ชื่อแฟ้มผลลัพธ์จะใช้เฉพาะตัวพิมพ์เล็กเท่านั้น"

#: ../thunar-plugin/tag-renamer.c:466 ../thunar-plugin/audio-tags-page.c:183
msgid "Unknown Artist"
msgstr "ศิลปินไม่ทราบชื่อ"

#: ../thunar-plugin/tag-renamer.c:474 ../thunar-plugin/audio-tags-page.c:209
msgid "Unknown Title"
msgstr "เพลงไม่ทราบชื่อ"

#. Edit tags action
#: ../thunar-plugin/tag-renamer.c:629
msgid "Edit _Tags"
msgstr "แก้ไขแ_ท็ก"

#: ../thunar-plugin/tag-renamer.c:629
msgid "Edit ID3/OGG tags of this file."
msgstr "แก้ไขแท็ก ID3/OGG ของแฟ้มนี้"

#: ../thunar-plugin/tag-renamer.c:648
msgid "Tag Help"
msgstr "วิธีใช้แท็ก"

#: ../thunar-plugin/tag-renamer.c:659
msgid "Artist"
msgstr "ศิลปิน"

#: ../thunar-plugin/tag-renamer.c:660
msgid "Album"
msgstr "อัลบั้ม"

#: ../thunar-plugin/tag-renamer.c:660
msgid "Genre"
msgstr "แนวเพลง"

#: ../thunar-plugin/tag-renamer.c:661
msgid "Track number"
msgstr "ลำดับเพลง"

#: ../thunar-plugin/tag-renamer.c:661
msgid "Year"
msgstr "ปี"

#: ../thunar-plugin/tag-renamer.c:662
msgid "Comment"
msgstr "หมายเหตุ"

#: ../thunar-plugin/tag-renamer.c:719
msgid "Audio Tags"
msgstr "แท็กเพลง"

#: ../thunar-plugin/audio-tags-page.c:196
msgid "Unknown Album"
msgstr "อัลบั้มไม่ทราบชื่อ"

#: ../thunar-plugin/audio-tags-page.c:328
msgid "<b>Track:</b>"
msgstr "<b>ลำดับ:</b>"

#: ../thunar-plugin/audio-tags-page.c:338
msgid "Enter the track number here."
msgstr "ป้อนลำดับเพลงที่นี่"

#: ../thunar-plugin/audio-tags-page.c:347
msgid "<b>Year:</b>"
msgstr "<b>ปี:</b>"

#: ../thunar-plugin/audio-tags-page.c:357
msgid "Enter the release year here."
msgstr "ป้อนปีที่เพลงออกที่นี่"

#: ../thunar-plugin/audio-tags-page.c:366
msgid "<b>Artist:</b>"
msgstr "<b>ศิลปิน:</b>"

#: ../thunar-plugin/audio-tags-page.c:373
msgid "Enter the name of the artist or author of this file here."
msgstr "ป้อนชื่อของศิลปินหรือผู้สร้างแฟ้มนี้ที่นี่"

#: ../thunar-plugin/audio-tags-page.c:382
msgid "<b>Title:</b>"
msgstr "<b>ชื่อเพลง:</b>"

#: ../thunar-plugin/audio-tags-page.c:389
msgid "Enter the song title here."
msgstr "ป้อนชื่อเพลงที่นี่"

#: ../thunar-plugin/audio-tags-page.c:397
msgid "<b>Album:</b>"
msgstr "<b>อัลบั้ม:</b>"

#: ../thunar-plugin/audio-tags-page.c:404
msgid "Enter the album/record title here."
msgstr "ป้อนชื่ออัลบั้มที่นี่"

#: ../thunar-plugin/audio-tags-page.c:412
msgid "<b>Comment:</b>"
msgstr "<b>หมายเหตุ:</b>"

#: ../thunar-plugin/audio-tags-page.c:419
msgid "Enter your comments here."
msgstr "ป้อนหมายเหตุของคุณที่นี่"

#: ../thunar-plugin/audio-tags-page.c:427
msgid "<b>Genre:</b>"
msgstr "<b>แนวเพลง:</b>"

#: ../thunar-plugin/audio-tags-page.c:434
msgid "Select or enter the genre of this song here."
msgstr "เลือกหรือป้อนแนวเพลงของเพลงนี้ที่นี่"

#: ../thunar-plugin/audio-tags-page.c:521
msgid "Audio"
msgstr "เสียง"

#. Set up the dialog
#: ../thunar-plugin/audio-tags-page.c:556
msgid "Edit Tags"
msgstr "แก้ไขแท็ก"

#: ../thunar-plugin/audio-tags-page.c:559
msgid "_Cancel"
msgstr "_ยกเลิก"

#. Create save button
#. Save button
#: ../thunar-plugin/audio-tags-page.c:570
#: ../thunar-plugin/audio-tags-page.c:1197
msgid "_Save"
msgstr "_บันทึก"

#. Create dialog
#: ../thunar-plugin/audio-tags-page.c:975
msgid "Audio Information"
msgstr "ข้อมูลเสียง"

#: ../thunar-plugin/audio-tags-page.c:978
msgid "_Close"
msgstr "ปิ_ด"

#: ../thunar-plugin/audio-tags-page.c:984
#, c-format
msgid "%d:%02d Minutes"
msgstr "%d:%02d นาที"

#: ../thunar-plugin/audio-tags-page.c:985
#, c-format
msgid "%d KBit/s"
msgstr "%d kBit/s"

#: ../thunar-plugin/audio-tags-page.c:986
#, c-format
msgid "%d Hz"
msgstr "%d Hz"

#: ../thunar-plugin/audio-tags-page.c:1010
msgid "<b>Filename:</b>"
msgstr "<b>ชื่อแฟ้ม:</b>"

#: ../thunar-plugin/audio-tags-page.c:1023
msgid "<b>Filesize:</b>"
msgstr "<b>ขนาดแฟ้ม:</b>"

#: ../thunar-plugin/audio-tags-page.c:1036
msgid "<b>MIME Type:</b>"
msgstr "<b>ชนิด MIME:</b>"

#: ../thunar-plugin/audio-tags-page.c:1049
msgid "<b>Bitrate:</b>"
msgstr "<b>อัตราบิต:</b>"

#: ../thunar-plugin/audio-tags-page.c:1062
msgid "<b>Samplerate:</b>"
msgstr "<b>อัตราสุ่ม:</b>"

#: ../thunar-plugin/audio-tags-page.c:1075
msgid "<b>Channels:</b>"
msgstr "<b>จำนวนช่อง:</b>"

#: ../thunar-plugin/audio-tags-page.c:1088
msgid "<b>Length:</b>"
msgstr "<b>ความยาว:</b>"

#. Info button
#: ../thunar-plugin/audio-tags-page.c:1188
msgid "_Information"
msgstr "_ข้อมูล"

#: ../thunar-plugin/audio-tags-page.c:1189
msgid "Display more detailed information about this audio file."
msgstr "แสดงรายละเอียดเกี่ยวกับแฟ้มเสียงนี้"

#: ../thunar-plugin/audio-tags-page.c:1198
msgid "Save audio tags."
msgstr "บันทึกแท็กเพลง"
